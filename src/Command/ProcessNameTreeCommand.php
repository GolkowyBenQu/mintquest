<?php

namespace App\Command;

use App\Service\DataStorage\JsonStorageService;
use App\Service\DataStorage\MemoryStorageService;
use App\Service\NameCombiner;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessNameTreeCommand extends Command
{
    /**
     * @var JsonStorageService
     */
    private $jsonStorageService;

    /**
     * @var NameCombiner
     */
    private $nameCombiner;

    public function __construct(
        JsonStorageService $jsonStorageService,
        NameCombiner $nameCombiner
    ) {
        $this->jsonStorageService = $jsonStorageService;
        $this->nameCombiner = $nameCombiner;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:combine-names');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['Combining names...', '=======================']);

        $listData = $this->jsonStorageService->getData('list.json');
        $treeData = $this->jsonStorageService->getData('tree.json');

        $result = $this->nameCombiner->combine($listData, $treeData);

        $this->jsonStorageService->saveData('result.json', $result);

        $output->writeln(json_encode($result));
        $output->writeln('Combine complete');
    }
}
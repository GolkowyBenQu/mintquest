<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('active_user');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'password'));
        $manager->persist($user);

        $user = new User();
        $user->setUsername('disabled_user');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'password'));
        $user->setDisabled(true);
        $manager->persist($user);

        for($i = 1; $i <= 100; $i++) {
            $user = new User();
            $user->setUsername('user_' . $i);
            $user->setPassword($this->passwordEncoder->encodePassword($user, 'password'));
            $user->setDisabled($i % 2);
            $manager->persist($user);
        }

        $manager->flush();
    }
}

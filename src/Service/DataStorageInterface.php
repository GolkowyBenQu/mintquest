<?php

namespace App\Service;

interface DataStorageInterface
{
    public function saveData(string $name, array $data): void;

    public function getData(string $name): array;
}
<?php

namespace App\Service;

class NameCombiner
{
    private $listArray = [];

    /**
     * @param array $listData
     * @param array $treeData
     * @return array
     */
    public function combine(array $listData, array $treeData): array
    {
        foreach ($listData as $listRow) {
            if (isset($listRow['category_id']) && isset($listRow['translations']['pl_PL']['name'])) {
                $this->listArray[$listRow['category_id']] = $listRow['translations']['pl_PL']['name'];
            }
        }

        return $this->getNames($treeData);
    }

    /**
     * @param array $treeData
     * @return array
     */
    private function getNames(array $treeData): array
    {
        $tempArray = [];

        foreach ($treeData as $treeRow) {
            if (isset($treeRow['id']) && isset($this->listArray[$treeRow['id']])) {
                $newRow = [
                    'id' => $treeRow['id'],
                    'name' => $this->listArray[$treeRow['id']],
                    'children' => [],
                ];

                if (count($treeRow['children'])) {
                    $newRow['children'] = $this->getNames($treeRow['children']);
                }

                $tempArray[] = $newRow;
            }

        }

        return $tempArray;
    }
}
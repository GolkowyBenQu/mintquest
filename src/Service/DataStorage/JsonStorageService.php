<?php

namespace App\Service\DataStorage;

use App\Service\DataStorageInterface;

class JsonStorageService implements DataStorageInterface
{
    public function saveData(string $name, array $data): void
    {
        file_put_contents(__DIR__.'/../../../public/' . $name, json_encode($data));
    }

    public function getData(string $name): array
    {
        $data = file_get_contents(__DIR__.'/../../../public/' . $name);

        return json_decode($data, true);
    }
}
<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_list")
     * @param UserRepository $userRepository
     * @param Request $request
     * @return Response
     */
    public function index(UserRepository $userRepository, Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 10);
        $userPagination = $userRepository->getUsers($page, $limit);

        return $this->render(
            'user/index.html.twig',
            [
                'users' => $userPagination,
                'currentPage' => $page,
                'limit' => $limit,
                'maxPages' => ceil($userPagination->count() / $limit),
            ]
        );
    }

    /**
     * @Route("/user/{id}/enable", name="user_enable")
     * @param User $user
     * @param UserRepository $userRepository
     * @param Request $request
     * @return RedirectResponse
     */
    public function enableUser(User $user, UserRepository $userRepository, Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 10);

        $userRepository->enableUser($user);

        return $this->redirectToRoute(
            'user_list',
            [
                'page' => $page,
                'limit' => $limit,
            ]
        );
    }

    /**
     * @Route("/user/{id}/disable", name="user_disable")
     * @param User $user
     * @param UserRepository $userRepository
     * @param Request $request
     * @return RedirectResponse
     */
    public function disableUser(User $user, UserRepository $userRepository, Request $request)
    {
        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 10);

        $userRepository->disableUser($user);

        return $this->redirectToRoute(
            'user_list',
            [
                'page' => $page,
                'limit' => $limit,
            ]
        );
    }
}

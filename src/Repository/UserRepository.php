<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUsers($page = 1, $limit = 10)
    {
        $qb = $this->createQueryBuilder('u')
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);

        return new Paginator($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     */
    public function enableUser(User $user)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->update()
            ->set('u.disabled', 0)
            ->where('u.id = ?1')
            ->setParameter(1, $user->getId())
            ->getQuery()
            ->execute();
    }

    /**
     * @param User $user
     */
    public function disableUser(User $user)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->update()
            ->set('u.disabled', 1)
            ->where('u.id = ?1')
            ->setParameter(1, $user->getId())
            ->getQuery()
            ->execute();
    }
}

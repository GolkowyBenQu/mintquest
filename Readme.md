# Mint User Manager #

This is user manager to list, enable or disable users. 
Allows to block user authentication, or process specific json data.

## 1. How to install? ##

You have to clone this repo
```bash
    git clone git@bitbucket.org:GolkowyBenQu/mintquest.git
```

After that, go to mintquest vatalog and install vendors
```bash
    cd mintquest
    composer install --dev
```

Copy and configure your local .env file
```bash
    cp .env .env.local
```

The most important line is:
```bash
    DATABASE_URL=mysql://user:password@192.168.1.14:3306/mint
```
Type here your proper access to local database.

Now you can create new database
```bash
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
```

And fill db by dummy data:
```bash
    php bin/console doctrine:fixtures:load
```

Now you can run application
```bash
    php bin/console server:run
```

And go to your browser to url http://127.0.0.1:8000

## 2. Start using ##

After completed installation, you should be redirected to login page. Type login and password here:
```php
login: active_user
password: password
```
for active user, or
```php
login: disabled_user
password: password
```
for disabled user.

You can disable or enable some user.

## 3. Names from json combining ##

There is one interesting feature to try. You can combine category names from one json file, with category tree structure from another one. Two files are there
```bash
public/list.json
public/tree.json
```

If you want to combine them, just type in consoole, in project root:
```bash
php bin/console app:combine-names
```

## 4. Last words ##

If you have some problems, questions, or you would like to improve some features, please contact me:
```bash
golec.przemyslaw@gmail.com
```